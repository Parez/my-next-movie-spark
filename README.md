# _Spark_Movies_Recommender_


## Prerequisites
- [Java](https://java.com/en/download/)
- [Gradle](https://gradle.org/)
- [Scala](https://www.scala-lang.org/)
- [MongoDB](https://www.mongodb.com/)

## Build and Demo process

### Clone the Repo
`git clone git@bitbucket.org:Parez/my-next-movie-spark.git`

### Build a self-contained jar
`./gradlew shadowJar`

To run the compiled jar run:

`java -jar ./build/libs/recommender-1.0-shadow.jar`

### Build and Run dev mode
`./gradlew clean run`

## Docker

The easiest way to run the application is to build its docker image and run a container locally.

Docker should be installed.

1. In the folder with Dockerfile run: `docker build sparkRecommender .`
2. After image is built run: `docker run -p 5000:5000 sparkRecommender`

The server should now be running on `localhost:5000`


### Database

By default the application will access the local MongoDB database on port `27017`.
To access database located in some other place you should provide a set of environment variables to the built jar or when running docker container.

Those are: 

1. `MONGO_URI`
2. `MONGO_PASS`
3. `MONGO_USERNAME`

Those are self-explanatory.

### Routes

The application contains 3 routes:

1. `/vectorize` - will access `movies_infos` collection in the database and transform all movies into vectorized format. Vectorized documents will be saved to `vector_data` collection. Sizes of vectors will be saved to `vector_size` collection. Once the data is saved you will need to restart the application to take latest changes into account.
2. `/similarities?movieId` - calculate top 10 similarities for a given movie. The job writes the results to the `similarities` collection.
3. `/recs?userId` - calculate top 30 recommendations for a given user. The job accesses `ratings` collections and writes the results to the `recommendations` collection.

To each of the routes an API key should be provided as an extra query parameter. For the testing purposes it is set to:
`vepuhoeri743rb9vbb3242093v820hewinsobcoh09qf09uvosdifoiohero8qg38gvqeouqg8`.


### Expectations

I am not expecting whoever will be checking the application to run all this as it's quite cumbersome to setup all the required tools, etc.
That's why I deployed everything to the cloud. The website can be accessed at https://my-next-movie.com.

The spark server itself is at https://spark.my-next-movie.com

