FROM java:8

COPY . /data

RUN cd /data; ./gradlew shadowJar;

EXPOSE 5000
ENV PORT 5000

WORKDIR /data

CMD ["java", "-jar", "./build/libs/recommender-1.0-shadow.jar"]