package baunov.recommender

import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark.sql.SparkSession

trait InitSpark {

  val mongoUri = sys.env.getOrElse("MONGO_URI", "")
  val mongoPass = sys.env.getOrElse("MONGO_PASS", "")
  val mongoUser = sys.env.getOrElse("MONGO_USERNAME", "")

  var uri = "mongodb://"+mongoUser+":"+mongoPass+"@"+mongoUri
  if (mongoUri == "") {
    uri = "mongodb://127.0.0.1/movie_db.movies_infos"
  }


  System.setProperty("org.mongodb.async.type", "netty")

  val sparkSession: SparkSession = SparkSession.builder()
                            .appName("Movie Recommender")
                            .master("local[*]")
                            .config("spark.driver.memory", "10g")
                            .config("spark.executor.memory", "10g")
                            .config("spark.mongodb.input.uri", uri)
                            .config("spark.mongodb.output.uri", uri)
                            .config("spark.mongodb.keep_alive_ms", "100000")
                            .getOrCreate()

  val sc = sparkSession.sparkContext
  sc.setCheckpointDir("/tmp/checkpoint/")
  val sqlContext = sparkSession.sqlContext

  private def init = {
    sc.setLogLevel("ERROR")
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
    LogManager.getRootLogger.setLevel(Level.ERROR)
  }
  init
  def close = {
    sparkSession.close()
  }
}
