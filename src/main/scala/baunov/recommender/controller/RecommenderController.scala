package baunov.recommender.controller

import baunov.recommender._
import utils.Logging
import akka.actor.ActorSystem
import baunov.recommender.Main.sc
import baunov.recommender.model.{MovieData, MovieVecData}
import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.ReadConfig
import org.apache.spark.sql.Dataset
import spray.routing.SimpleRoutingApp


/**
  * Created by baunov on 13/04/2018.
  */
object RecommenderController extends SimpleRoutingApp with CORSSupport with Logging{

  implicit val system = ActorSystem("ActorSystem")


  private val API_KEY = sys.env.getOrElse("API_KEY", "vepuhoeri743rb9vbb3242093v820hewinsobcoh09qf09uvosdifoiohero8qg38gvqeouqg8")

  def run(serverPort: Int): Unit = {

    val moviesSizeConfig = ReadConfig(Map("collection" -> "vector_size"), Some(ReadConfig(sc)))
    val vectorSizeRecord = MongoSpark.load(sc, moviesSizeConfig).toDF().rdd.take(1).last

    val movieDataDs: Dataset[MovieData] = loadMovies()

    val sparseVecMoviesDataDs: Dataset[MovieVecData] = toSparseVectorDS(movieDataDs, vectorSizeRecord).cache()

    logger.info("Launching REST serves[port=%d]".format(serverPort))

    startServer(interface = "0.0.0.0", port = serverPort) {

      cors {
        path("recs") {
          get {
            parameter('userId.as[String], 'apiKey.as[String]) { (userId, apiKey) =>
              if (apiKey == API_KEY) {
                RecommenderService.calculate(sparseVecMoviesDataDs, userId)
                complete("Ok")
              } else {
                complete("Wrong API KEY")
              }

            }
          }
        } ~
          path("vectorize") {
            get {
              parameter('apiKey.as[String]) { (apiKey) =>
                if (apiKey == API_KEY) {
                  MoviesVectorizer.calculate()
                  complete("Ok")
                } else {
                  complete("Wrong API KEY")
                }

              }
            }
          } ~
          path("similarities") {
            get {
              parameter('movieId.as[Int], 'apiKey.as[String]) { (movieId, apiKey) =>
                if (apiKey == API_KEY) {
                  SimilaritiesService.calculate(sparseVecMoviesDataDs, movieId)
                  complete("Ok")
                } else {
                  complete("Wrong API KEY")
                }

              }
            }
          }

      }

    }
  }
}
