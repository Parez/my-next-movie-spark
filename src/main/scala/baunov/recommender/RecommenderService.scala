package baunov.recommender

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.{ReadConfig, WriteConfig}
import org.apache.spark.sql.functions.{col, lit}
import Main.{sc, sparkSession}
import baunov.recommender.model._
import com.mongodb.spark.sql.fieldTypes.ObjectId
import org.apache.spark.sql.{DataFrame, Dataset}
import org.apache.spark.mllib.linalg.SparseVector
import org.apache.spark.sql.functions._
import org.bson.Document
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema

import scala.concurrent.Future
import scala.collection.Map
import sparkSession.implicits._

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by baunov on 20/03/2018.
  */
object RecommenderService {
  private val MAX_RECOMMENDATIONS: Int = 30
  private val NEGATIVE_VOTE_WEIGHT: Double = 1.5
  private val MILLIS_PER_DAY: Double = 86400000
  private val DAYS_IN_YEAR: Double = 365
  private val DECREASE_PER_YEAR: Double = 0.5

  private var minRating: Double = 10

  def calculate(sparseVecMoviesDataDs: Dataset[MovieVecData], userId: String) = Future {
    val ratingsConfig = ReadConfig(Map("collection" -> "ratings"), Some(ReadConfig(sc)))
    val writeConfig = WriteConfig(Map("collection" -> "recommendations"), Some(WriteConfig(sc)))

    val ratingsDf = MongoSpark.load(sc, ratingsConfig)
      .withPipeline(Seq(Document.parse("{ $match: { userId : '"+ userId +"'} }")))
      .toDF().cache()

    ratingsDf.printSchema()

    val ratingsList = ratingsDf.select("movieId").collect().toList.map(i => i(0))



    val sparseVecUserData: Dataset[UserPreferencesVecData] = generateUserMoviePreferences(userId, ratingsDf, sparseVecMoviesDataDs)

    val userPreferences: UserPreferencesVecData = combineUserPreferences(sparseVecUserData.collect().toList)

    val filteredVecMoviesData: Dataset[MovieVecData] = sparseVecMoviesDataDs.filter(col("Rating") >= this.minRating)

    val scoresDS: Dataset[ScoresData] = calculateScores(userPreferences, filteredVecMoviesData.filter(!col("imdbID")
      .isin(ratingsList:_*)))

    val finalDS = scoresDS.sort(desc("score"))
      .limit(MAX_RECOMMENDATIONS)
      .withColumn("userId", lit(userId))
      .as[Recommendation]

    MongoSpark.save(finalDS, writeConfig)
  }

  def combineUserPreferences(prefData: List[UserPreferencesVecData]): UserPreferencesVecData = {
    prefData.reduce((p1, p2) => {
      combinePrefData(p1, p2)
    })
  }

  def combinePrefData(p1: UserPreferencesVecData, p2: UserPreferencesVecData): UserPreferencesVecData = {
    val title = toSpark(toBreeze(p1.Title.toSparse) + toBreeze(p2.Title.toSparse)).toSparse
    val decade = toSpark(toBreeze(p1.Decade.toSparse) + toBreeze(p2.Decade.toSparse)).toSparse
    val plot = toSpark(toBreeze(p1.Plot.toSparse) + toBreeze(p2.Plot.toSparse)).toSparse
    val genres = toSpark(toBreeze(p1.Genres.toSparse) + toBreeze(p2.Genres.toSparse)).toSparse
    val actors = toSpark(toBreeze(p1.Actors.toSparse) + toBreeze(p2.Actors.toSparse)).toSparse
    val countries = toSpark(toBreeze(p1.Countries.toSparse) + toBreeze(p2.Countries.toSparse)).toSparse
    val writers = toSpark(toBreeze(p1.Writers.toSparse) + toBreeze(p2.Writers.toSparse)).toSparse
    val directors = toSpark(toBreeze(p1.Directors.toSparse) + toBreeze(p2.Directors.toSparse)).toSparse
    val productions = toSpark(toBreeze(p1.Productions.toSparse) + toBreeze(p2.Productions.toSparse)).toSparse
    UserPreferencesVecData(p1.userId, title, decade, plot, genres, actors, countries, writers, directors, productions)
  }

  def generateUserMoviePreferences(userId: String, ratingsDf: DataFrame, moviesDataDS: Dataset[MovieVecData]): Dataset[UserPreferencesVecData] = {
    val timestamps = ratingsDf.rdd.map((r: Row) => Integer.parseInt(getObjectId(r).substring(0,8), 16)*1000)
    val maxTimestamp: Double = timestamps.max()

    ratingsDf.join(moviesDataDS, ratingsDf.col("movieId") === moviesDataDS.col("imdbID"))
      .map( (r: Row) => {
        val timestamp: Long = Integer.parseInt(getObjectId(r).substring(0,8), 16)*1000

        val daysBetween: Double = Math.floor((maxTimestamp - timestamp) / MILLIS_PER_DAY)

        val voteWeight: Double = Math.pow(DECREASE_PER_YEAR, daysBetween / DAYS_IN_YEAR)


        var vote: Double = r.getAs[Int]("rating").toDouble * voteWeight
        if (vote < 0) {
          vote *= NEGATIVE_VOTE_WEIGHT
        }

        val rating = r.getAs[Double]("Rating")
        if (vote > 0 && rating < this.minRating) {
          this.minRating = rating
        }

        val title: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Title")) * vote).toSparse
        val decade: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Decade")) * vote).toSparse
        val plot: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Plot")) * vote).toSparse
        val genres: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Genres")) * vote).toSparse
        val actors: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Actors")) * vote).toSparse
        val countries: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Countries")) * vote).toSparse
        val writers: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Writers")) * vote).toSparse
        val directors: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Directors")) * vote).toSparse
        val productions: SparseVector = toSpark(toBreeze(r.getAs[SparseVector]("Productions")) * vote).toSparse
        UserPreferencesVecData(userId, title, decade, plot, genres, actors, countries, writers, directors, productions)
      })
  }

  def getObjectId(row: Row): String = {
    row.getAs[GenericRowWithSchema](1).toSeq.head.asInstanceOf[String]
  }
}
