package baunov.recommender

import breeze.linalg.{DenseVector => BDV, SparseVector => BSV, Vector => BV}
import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.{WriteConfig, _}
import org.apache.spark.ml.linalg.{DenseVector, SparseVector}
import org.apache.spark.sql._
import org.apache.spark.sql.functions._

import scala.collection._
import scala.math.sqrt


/**
  * Represents a Users movie rating
  */
final case class UserMovieRating(user_id: Int,
                                 movie_id: Int,
                                 rating: Double)

final case class MovieRatingsVector(movie_id: Long, ratings: Seq[Double])

final case class MovieSimilarity(movie_id1: Long, movie_id2: Long, similarity: Double, corated: Long, strength: Double)


object Main extends InitSpark {

  type MovieRating = (Int, Double)
  type UserRatingPair = (Int, (MovieRating, MovieRating))



  val cosineUdf = udf {
    (arr1: SparseVector, arr2: SparseVector) => cosine(arr1 , arr2)
  }




  /*def main(args: Array[String]) = {

    val moviesSizeConfig = ReadConfig(Map("collection" -> "vector_size"), Some(ReadConfig(sc)))

    val vectorSizeRecord = MongoSpark.load(sc, moviesSizeConfig).toDF().rdd.take(1).last

    //combineMovieLinks()

    val vectorSizeConfig = WriteConfig(Map("collection" -> "vector_size"), Some(WriteConfig(sc)))
    val writeConfig = WriteConfig(Map("collection" -> "vector_data"), Some(WriteConfig(sc)))


    // MongoSpark.save(sizeDf, vectorSizeConfig)
    // MongoSpark.save(dataDf, writeConfig)

    // RecommenderService.calculate("5a99aa6f6cd2001c4cb6e2ed")


    close
  }*/




  def calcMovieContentSimilarities(pairsDf: DataFrame): DataFrame = {
    pairsDf
      .filter(col("movie_id1") =!= col("movie_id2"))
      .withColumn("TitleSim", cosineUdf(col("TitleVec1"), col("TitleVec2"))).drop("TitleVec1", "TitleVec2")
      .withColumn("PlotSim", cosineUdf(col("PlotVec1"), col("PlotVec2"))).drop("PlotVec1", "PlotVec2")
      .withColumn("GenresSim", cosineUdf(col("GenresVec1"), col("GenresVec2"))).drop("GenresVec1", "GenresVec2")
      .withColumn("ActorsSim", cosineUdf(col("ActorsVec1"), col("ActorsVec2"))).drop("ActorsVec1", "ActorsVec2")
      .withColumn("CountriesSim", cosineUdf(col("CountriesVec1"), col("CountriesVec2"))).drop("CountriesVec1", "CountriesVec2")
      .withColumn("DirectorsSim", cosineUdf(col("DirectorsVec1"), col("DirectorsVec2"))).drop("DirectorsVec1", "DirectorsVec2")
  }



  def makeMovieDataPairs(vecDf: DataFrame): DataFrame = {
    vecDf
      .withColumnRenamed("TitleVec", "TitleVec1")
      .withColumnRenamed("PlotVec", "PlotVec1")
      .withColumnRenamed("GenresVec", "GenresVec1")
      .withColumnRenamed("ActorsVec", "ActorsVec1")
      .withColumnRenamed("CountriesVec", "CountriesVec1")
      .withColumnRenamed("DirectorsVec", "DirectorsVec1")
      .withColumnRenamed("imdbID", "movie_id1")
      .crossJoin(vecDf
        .withColumnRenamed("TitleVec", "TitleVec2")
        .withColumnRenamed("PlotVec", "PlotVec2")
        .withColumnRenamed("GenresVec", "GenresVec2")
        .withColumnRenamed("ActorsVec", "ActorsVec2")
        .withColumnRenamed("CountriesVec", "CountriesVec2")
        .withColumnRenamed("DirectorsVec", "DirectorsVec2")
        .withColumnRenamed("imdbID", "movie_id2"))
  }



  def combineMovieLinks(): Unit = {
    val movieLinksConfig = ReadConfig(Map("collection" -> "links"), Some(ReadConfig(sc)))
    val moviesConfig = ReadConfig(Map("collection" -> "movies"), Some(ReadConfig(sc)))

    val outConfig = WriteConfig(Map("collection" -> "movie_ids"), Some(WriteConfig(sc)))

    val movieLinksDF = MongoSpark
      .load(sc, movieLinksConfig).toDF().select("movieId", "imdbId")

    val moviesDF = MongoSpark
      .load(sc, moviesConfig).toDF()

    val movies = moviesDF.as("movies")
    val links = movieLinksDF.as("links")

    val outDF = movies.join(links, links("movieId") === movies("movieId"), "inner")
      .select("imdbId", "title")
      .withColumnRenamed("imdbId", "movie_id")

    MongoSpark.save(outDF, outConfig)
  }

  /*def calculateCollabSimilarities(moviesRatingsDF: DataFrame, movieLinksDF: DataFrame): Unit = {
    val ratings = moviesRatingsDF.as("ratings")
    val links = movieLinksDF.as("links")

    val imdbRatingsDF = links.join(ratings, links("movieId") === ratings("movieId"), "inner")
      .select("imdbId", "userId", "rating")
      .withColumnRenamed("imdbId", "movieId")

    val ratingsRdd = imdbRatingsDF.rdd.map(row => {
      (row.getAs[Int]("userId"), (row.getAs[Int]("movieId"), row.getAs[Double]("rating")))
    })

    // Emit every movie rated together by the same user.
    // Self-join to find every combination.
    val joinedRatings = ratingsRdd.join(ratingsRdd)

    // At this point our RDD consists of userID => ((movieID, rating), (movieID, rating))
    val ratingPairsRdd = joinedRatings.map(makePairs).partitionBy(new HashPartitioner(1000))

    val moviePairRatings = ratingPairsRdd.groupByKey()

    val moviePairSimilarities = moviePairRatings.mapValues(computePearsonSimilarity)
      .filter(sim => sim._1._1 != sim._1._2)


    val movieSimilaritiesDS:Dataset[MovieSimilarity] = moviePairSimilarities.map(sim => {
      MovieSimilarity(
        movie_id1 = sim._1._1,
        movie_id2 = sim._1._2,
        similarity = sim._2._1,
        corated = sim._2._2,
        strength = sim._2._3)
    }).toDS()

    val collabMinCorated = 30
    val combineSimilarities = udf {
      (corated:Integer, collab: Double, content: Double) => {
        collab * content
      }
    }
  }*/

  def toBreeze(v: SparseVector): BSV[Double] = v match {
    case SparseVector(size, indices, values) => {
      new BSV[Double](indices, values, size)
    }
  }

  def toSpark(v: BV[Double]) = v match {
    case v: BDV[Double] => new DenseVector(v.toArray)
    case v: BSV[Double] => new SparseVector(v.length, v.index, v.data)
  }

  def correlation(seq1: Seq[Double], seq2: Seq[Double]): Double = {
    //val normalized1 = normalizeSeq(a)
    //val normalized2 = normalizeSeq(b)
    val sim = cosineSimilarity(seq1, seq2)
    if (sim.isNaN) return 0.0
    sim
  }


  def cosine(vec1: SparseVector, vec2: SparseVector): Double = {
    val a: BSV[Double] = toBreeze(vec1)
    val b: BSV[Double] = toBreeze(vec2)

    if (a.length != b.length)
      throw new IllegalArgumentException("Vectors not of the same length.")

    val dot = a.dot(b)


    val maga = magnitude(a)
    val magb = magnitude(b)

    dot / (maga * magb)
  }

  def magnitude(vec: BSV[Double]): Double = {
    var offset = 0
    var mag:Double = 0
    while(offset < vec.activeSize) {
      val value = vec.valueAt(offset)
      // use index and value
      mag += value * value
      offset += 1
    }
    math.sqrt(mag)
  }

  def pearson(a: BV[Double], b: BV[Double]): Double = {
    if (a.length != b.length)
      throw new IllegalArgumentException("Vectors not of the same length.")

    val n = a.length

    val dot = a.dot(b)
    val adot = a.dot(a)
    val bdot = b.dot(b)
    val amean = breeze.stats.mean(a)
    val bmean = breeze.stats.mean(b)

    // See Wikipedia http://en.wikipedia.org/wiki/Pearson_product-moment_correlation_coefficient#For_a_sample
    (dot - n * amean * bmean ) / ( sqrt(adot - n * amean * amean)  * sqrt(bdot - n * bmean * bmean) )
  }

  def cosineSimilarity(x: Seq[Double], y: Seq[Double]): Double = {
    require(x.size == y.size)
    dotProduct(x, y)/(magnitude(x) * magnitude(y))
  }

  /*
   * Return the dot product of the 2 arrays
   * e.g. (a[0]*b[0])+(a[1]*a[2])
   */
  def dotProduct(x: Seq[Double], y: Seq[Double]): Double = {
    (for((a, b) <- x zip y) yield a * b) sum
  }

  /*
   * Return the magnitude of an array
   * We multiply each element, sum it, then square root the result.
   */
  def magnitude(x: Seq[Double]): Double = {
    math.sqrt(x map(i => i*i) sum)
  }
}


/*package template.spark

import org.apache.spark.sql.functions._
import org.apache.spark.sql._

import scala.collection._
import com.mongodb.spark.MongoSpark
import template.spark.Main.spark
import spark.implicits._
import org.apache.log4j.{Level, LogManager}
import breeze.linalg._
import breeze.linalg.functions.cosineDistance
import breeze.stats._
import com.mongodb.spark.config.ReadConfig
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.distributed.{MatrixEntry, RowMatrix}
import org.apache.spark.ml.feature.Imputer


import scala.math.sqrt

/**
  * Represents a Users movie rating
  */
final case class UserMovieRating(user_id: Int,
                           movie_id: Int,
                           rating: Double)

final case class MovieRatingsVector(movie_id: Long, ratings: Seq[Double])

final case class MovieSimilarity(movie_id1: Long, movie_id2: Long, similarity: Double)

object Main extends InitSpark {
  def main(args: Array[String]) = {

    val vec1 = Vector[Double](1,2,1,2)
    val vec2 = Vector[Double](2,1,2,1)

    println(pearson(vec1, vec2))

    val log = LogManager.getRootLogger
    log.setLevel(Level.WARN)
    // Load the movie rating data
    val moviesRatingsConfig = ReadConfig(Map("collection" -> "movie_ratings"), Some(ReadConfig(sc)))
    val moviesConfig = ReadConfig(Map("collection" -> "movies"), Some(ReadConfig(sc)))
    val moviesLinksConfig = ReadConfig(Map("collection" -> "movie_links"), Some(ReadConfig(sc)))

    val moviesDF = MongoSpark
      .load(sc, moviesConfig).toDF().select("movieId", "title","genres")

    /*val genresDF = moviesDF
      .withColumn("genres", org.apache.spark.sql.functions.split(moviesDF("genres"), """\|""").cast("array<string>"))*/

    val movieLinksDF = MongoSpark
      .load(sc, moviesLinksConfig).toDF().select("movieId", "imdbId")


    val moviesRatingsDF = MongoSpark
      .load(sc, moviesRatingsConfig).toDF().select("movieId", "userId","rating")


    val imdbRatingsDF = movieLinksDF
      .join(moviesRatingsDF, Seq("movieId")).drop("movieId").select("imdbId", "userId", "rating")

    //imdbRatingsDF.show()

    val movieRatingsDF = imdbRatingsDF
      .groupBy("imdbId")
      .pivot("userId")
      .max("rating").na.fill(0)

    //movieRatingsDF.show()
    //

    //val fullMovieDF = movieRatingsDF;//.join(genresDF, "movieId").join(movieLinksDF, "movieId")

    val ratingColumns = movieRatingsDF.drop("imdbId").columns

    val movieRatingsDS:Dataset[MovieRatingsVector] = movieRatingsDF
      .select( col("imdbId").as("movie_id"), array(ratingColumns.map(x => col(x)): _*).as("ratings") )
      .as[MovieRatingsVector]

    val moviePairs = movieRatingsDS.withColumnRenamed("ratings", "ratings1")
      .withColumnRenamed("movie_id", "movie_id1")
      .crossJoin(movieRatingsDS.withColumnRenamed("ratings", "ratings2").withColumnRenamed("movie_id", "movie_id2"))

    moviePairs.printSchema()

    val corrUdf = udf {
      (arr1: Seq[Double], arr2: Seq[Double]) => correlation(arr1 , arr2)
    }

    val movieSimilarities = moviePairs
      .select($"movie_id1", $"movie_id2", corrUdf($"ratings1", $"ratings2") as "similarity")
      .filter(col("movie_id1") =!= col("movie_id2"))
      .filter(col("similarity") > 0.3)

    movieSimilarities.show(200)
    log.warn("I'm done")

    MongoSpark.save(movieSimilarities)



    close
  }

  def correlation(seq1: Seq[Double], seq2: Seq[Double]): Double = {
    val normalized1 = normalizeSeq(seq1)
    val normalized2 = normalizeSeq(seq2)

    cosineSimilarity(normalized1, normalized2)
  }

  def normalizeSeq(seq: Seq[Double]): Seq[Double] = {
    var sum:Double = 0
    var count:Double = 0
    seq.filter(x => x != 0).foreach(x => {
      sum += x
      count += 1
    })
    val avg = sum / count
    seq.map(x => {
      if( x != 0 ) {
        x - avg
      } else {
        0.0
      }
    })
  }

  def cosineSimilarity(x: Seq[Double], y: Seq[Double]): Double = {
    require(x.size == y.size)
    dotProduct(x, y)/(magnitude(x) * magnitude(y))
  }

  /*
   * Return the dot product of the 2 arrays
   * e.g. (a[0]*b[0])+(a[1]*a[2])
   */
  def dotProduct(x: Seq[Double], y: Seq[Double]): Double = {
    (for((a, b) <- x zip y) yield a * b) sum
  }

  /*
   * Return the magnitude of an array
   * We multiply each element, sum it, then square root the result.
   */
  def magnitude(x: Seq[Double]): Double = {
    math.sqrt(x map(i => i*i) sum)
  }


  def cosine(a: Vector[Double], b: Vector[Double]): Double = {
    if (a.length != b.length)
      throw new IllegalArgumentException("Vectors not of the same length.")

    val (vec1, vec2) = getCommonVectors(a,b)
    cosineDistance(vec1, vec2)
  }

  def getCommonVectors(a: Vector[Double], b: Vector[Double]): (Vector[Double], Vector[Double]) = {
    var ar1:Array[Double] = Array[Double]()
    var ar2:Array[Double] = Array[Double]()

    for(i <- 0 until a.length) {
      if ( a(i) != 0 && b(i) != 0 ) {
        ar1 :+= a(i)
        ar2 :+= b(i)
      }
    }

    (Vector(ar1), Vector(ar2))
  }

  def centreVector(vec: Vector[Double]): Vector[Double] = {
    var mean:Double = 0.0
    for(i <- 0 until vec.length) {
      mean += vec(i)
    }
    mean /= vec.length
    vec.map((a) => a - mean)
  }

  /**
    * Works for all all vectors. Scales in O(length)
    */
  def pearson(a: Vector[Double], b: Vector[Double]): Double = {
    if (a.length != b.length)
      throw new IllegalArgumentException("Vectors not of the same length.")

    val n = a.length

    val dot = a.dot(b)
    val adot = a.dot(a)
    val bdot = b.dot(b)
    val amean = breeze.stats.mean(a)
    val bmean = breeze.stats.mean(b)

    // See Wikipedia http://en.wikipedia.org/wiki/Pearson_product-moment_correlation_coefficient#For_a_sample
    (dot - n * amean * bmean ) / ( sqrt(adot - n * amean * amean)  * sqrt(bdot - n * bmean * bmean) )
  }
}*/
