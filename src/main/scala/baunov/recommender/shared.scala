package baunov

import baunov.recommender.Main.{sc, sparkSession}
import baunov.recommender.model._
import breeze.linalg.{DenseVector => BDV, SparseVector => BSV, Vector => BV}
import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.ReadConfig
import org.apache.spark.mllib.linalg.{DenseVector, SparseVector, Vectors}
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.sql.functions.udf
import org.bson.Document

import scala.collection.Map
import scala.collection.immutable.ListMap
import sparkSession.implicits._

/**
  * Created by baunov on 27/04/2018.
  */
package object recommender {

  private val titleWeight: Double = 0.4
  private val plotWeight: Double = 1
  private val actorsWeight: Double = 1.5
  private val genresWeight: Double = 1.5
  private val directorsWeight: Double = 1.5
  private val writersWeight: Double = 1.5
  private val productionsWeight: Double = 0.7
  private val decadeWeight: Double = 1
  private val countriesWeight: Double = 0.7

  val MAX_RECOMMENDATIONS: Int = 30
  val MAX_SIMILARITIES: Int = 10

  val mapToSparseVectorUdf = udf {
    (myMap: Map[Int, Double], size: Int) => {
      mapToSparseVector(myMap, size)
    }
  }

  val intersectionCosineUdf = udf {
    (arr1: SparseVector, arr2: SparseVector) => intersectionCosine(arr1 , arr2)
  }

  val combineUdf = udf {
    (title: Double,
     plot: Double,
     actors: Double,
     directors: Double,
     genres: Double,
     countries: Double,
     writers: Double,
     productions: Double,
     decade: Double) =>
      title * titleWeight +
        plot * plotWeight +
        actors * actorsWeight +
        directors * directorsWeight +
        genres * genresWeight +
        countries * countriesWeight +
        writers * writersWeight +
        productions * productionsWeight +
        decade * decadeWeight

  }

  def toBreeze(v: SparseVector): BSV[Double] = v match {
    case SparseVector(size, indices, values) => {
      new BSV[Double](indices, values, size)
    }
  }

  def toSpark(v: BV[Double]) = v match {
    case v: BDV[Double] => new DenseVector(v.toArray)
    case v: BSV[Double] => new SparseVector(v.length, v.index, v.data)
  }

  def magnitude(vec: BSV[Double]): Double = {
    var offset = 0
    var mag:Double = 0
    while(offset < vec.activeSize) {
      val value = vec.valueAt(offset)
      // use index and value
      mag += value * value
      offset += 1
    }
    math.sqrt(mag)
  }

  def intersectionCosine(movie1Vec: SparseVector, movie2Vec: SparseVector): Double = {
    val a: BSV[Double] = toBreeze(movie1Vec)
    val b: BSV[Double] = toBreeze(movie2Vec)

    var dot: Double = 0
    var offset: Int = 0
    while( offset < a.activeSize) {
      val index: Int = a.indexAt(offset)
      val value: Double = a.valueAt(offset)

      dot += value * b(index)
      offset += 1
    }

    val bReduced: BSV[Double] = new BSV(a.index, a.index.map(i => b(i)), a.index.length)
    val maga: Double = magnitude(a)
    val magb: Double = magnitude(bReduced)

    if (maga == 0 || magb == 0)
      return 0
    else
      return dot / (maga * magb)
  }

  def documentToMap(doc: Document): Map[Int, Double] = {
    doc.keySet().toArray.map(key => {
      (key.toString.toInt, doc.get(key).toString.toDouble)
    }).toMap
  }

  def mapToSparseVector(myMap: Map[Int, Double], size: Int): SparseVector = {
    val sorted = ListMap(myMap.toSeq.sortBy(_._1):_*)
    val keys: Array[Int] = sorted.keys.toArray
    val values: Array[Double] = sorted.values.toArray
    Vectors.sparse(size, keys, values).toSparse
  }

  def toSparseVectorDS(ds: Dataset[MovieData], sizeRecord: Row): Dataset[MovieVecData] = {
    ds.rdd.map( (movieData: MovieData) => {
      val imdbID: Int = movieData.imdbID
      val title: SparseVector = mapToSparseVector(movieData.Title, sizeRecord.getAs[Int]("Title"))
      val decade: SparseVector = mapToSparseVector(movieData.Decade, sizeRecord.getAs[Int]("Decade"))
      val plot: SparseVector = mapToSparseVector(movieData.Plot, sizeRecord.getAs[Int]("Plot"))
      val genres: SparseVector = mapToSparseVector(movieData.Genres, sizeRecord.getAs[Int]("Genres"))
      val actors: SparseVector = mapToSparseVector(movieData.Actors, sizeRecord.getAs[Int]("Actors"))
      val countries: SparseVector = mapToSparseVector(movieData.Countries, sizeRecord.getAs[Int]("Countries"))
      val writers: SparseVector = mapToSparseVector(movieData.Writers, sizeRecord.getAs[Int]("Writers"))
      val directors: SparseVector = mapToSparseVector(movieData.Directors, sizeRecord.getAs[Int]("Directors"))
      val productions: SparseVector = mapToSparseVector(movieData.Productions, sizeRecord.getAs[Int]("Productions"))
      val rating: Double = movieData.imdbRating
      MovieVecData(imdbID, rating, title, decade, plot, genres, actors, countries, writers, directors, productions)
    }).toDS()
  }

  def combineSimilarities(title: Double,
                          plot: Double,
                          actors: Double,
                          directors: Double,
                          genres: Double,
                          countries: Double,
                          writers: Double,
                          productions: Double,
                          decade: Double): Double = {
    return title * titleWeight +
      plot * plotWeight +
      actors * actorsWeight +
      directors * directorsWeight +
      genres * genresWeight +
      countries * countriesWeight +
      writers * writersWeight +
      productions * productionsWeight +
      decade * decadeWeight
  }

  def calculateScores(vecData: VecData, dataset: Dataset[MovieVecData]): Dataset[ScoresData] = {
    dataset.rdd.map((movie: MovieVecData) => {

      val id = movie.imdbID
      val genres = intersectionCosine(movie.Genres.toSparse, vecData.Genres.toSparse)

      if (genres <= 0) {
        ScoresData(id, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
      } else {
        val title = intersectionCosine(movie.Title.toSparse, vecData.Title.toSparse)
        val plot = intersectionCosine(movie.Plot.toSparse, vecData.Plot.toSparse)
        val actors = intersectionCosine(movie.Actors.toSparse, vecData.Actors.toSparse)
        val directors = intersectionCosine(movie.Directors.toSparse, vecData.Directors.toSparse)

        val countries = intersectionCosine(movie.Countries.toSparse, vecData.Countries.toSparse)
        val writers = intersectionCosine(movie.Writers.toSparse, vecData.Writers.toSparse)
        val productions = intersectionCosine(movie.Productions.toSparse, vecData.Productions.toSparse)
        val decade = intersectionCosine(movie.Decade.toSparse, vecData.Decade.toSparse)
        val score = combineSimilarities(title, plot, actors, directors, genres, countries, writers, productions, decade)

        ScoresData(id, title, decade, plot, genres, actors, countries, writers, directors, productions, score)
      }
    }).toDS()
  }

  def loadMovies(): Dataset[MovieData] = {
    val moviesDataConfig = ReadConfig(Map("collection" -> "vector_data"), Some(ReadConfig(sc)))

    MongoSpark
      .load(sc, moviesDataConfig)
      .rdd.map((doc: Document) => {
      MovieData(
        doc.get("imdbID").asInstanceOf[Int],
        documentToMap(doc.get("Title").asInstanceOf[Document]),
        documentToMap(doc.get("Decade").asInstanceOf[Document]),
        documentToMap(doc.get("Plot").asInstanceOf[Document]),
        documentToMap(doc.get("Genres").asInstanceOf[Document]),
        documentToMap(doc.get("Actors").asInstanceOf[Document]),
        documentToMap(doc.get("Countries").asInstanceOf[Document]),
        documentToMap(doc.get("Writers").asInstanceOf[Document]),
        documentToMap(doc.get("Directors").asInstanceOf[Document]),
        documentToMap(doc.get("Productions").asInstanceOf[Document]),
        doc.get("imdbRating").asInstanceOf[Double]
      )
    }).toDS()
  }

}
