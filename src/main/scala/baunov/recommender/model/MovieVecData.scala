package baunov.recommender.model

import org.apache.spark.mllib.linalg.Vector

/**
  * Created by baunov on 27/04/2018.
  */
final case class MovieVecData(imdbID: Int,
                              Rating: Double,
                              Title: Vector,
                              Decade: Vector,
                              Plot: Vector,
                              Genres: Vector,
                              Actors: Vector,
                              Countries: Vector,
                              Writers: Vector,
                              Directors: Vector,
                              Productions: Vector
                             ) extends VecData