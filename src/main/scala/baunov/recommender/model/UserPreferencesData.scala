package baunov.recommender.model

import scala.collection.Map

/**
  * Created by baunov on 27/04/2018.
  */
final case class UserPreferencesData(userId: String,
                                     Title: Map[Int, Double],
                                     Decade: Map[Int, Double],
                                     Plot: Map[Int, Double],
                                     Genres: Map[Int, Double],
                                     Actors: Map[Int, Double],
                                     Countries: Map[Int, Double],
                                     Writers: Map[Int, Double],
                                     Directors: Map[Int, Double],
                                     Productions: Map[Int, Double]
                                    )
