package baunov.recommender.model

import org.apache.spark.mllib.linalg.Vector

/**
  * Created by baunov on 27/04/2018.
  */
trait VecData {
  def Title: Vector
  def Decade: Vector
  def Plot: Vector
  def Genres: Vector
  def Actors: Vector
  def Countries: Vector
  def Writers: Vector
  def Directors: Vector
  def Productions: Vector
}