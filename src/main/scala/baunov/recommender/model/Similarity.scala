package baunov.recommender.model

/**
  * Created by baunov on 28/04/2018.
  */
final case class Similarity(movie_id1: Int, movie_id2: Int, score: Double)
