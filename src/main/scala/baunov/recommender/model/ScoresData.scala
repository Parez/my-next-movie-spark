package baunov.recommender.model

/**
  * Created by baunov on 27/04/2018.
  */
case class ScoresData(imdbID: Int,
                            TitleScore: Double,
                            DecadeScore: Double,
                            PlotScore: Double,
                            GenresScore: Double,
                            ActorsScore: Double,
                            CountriesScore: Double,
                            WritersScore: Double,
                            DirectorsScore: Double,
                            ProductionsScore: Double,
                            score: Double
                           )
