package baunov.recommender

import baunov.recommender.Main.{sc, sparkSession}
import breeze.linalg.{DenseVector => BDV, SparseVector => BSV, Vector => BV}
import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.{ReadConfig, WriteConfig}
import org.apache.spark.ml.feature._
import org.apache.spark.ml.linalg.{DenseVector, SparseVector}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, lit, udf, _}
import sparkSession.implicits._

import scala.collection.Map
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

final case class MoviesVectorsInfo(vectors: DataFrame, sizes: DataFrame)

/**
  * Created by baunov on 20/03/2018.
  */
object MoviesVectorizer {

  val sparseVectorToMapUdf = udf {
    (vec: SparseVector) => {
      val myMap: Map[String, Double] = vec.indices
        .map((index) => (index.toString, vec.toArray(index)))
        .toMap

      myMap
    }
  }

  val normalizeBinaryVectorUdf = udf {
    (vec: SparseVector) => {
      new SparseVector(vec.size, vec.indices, vec.values.map(v => v/Math.sqrt(vec.values.length)))
    }
  }

  val yearToDecadeUdf = udf {
    (year: Int) => Array(Math.floor(year/10))
  }

  val emptyArrayUdf = udf(() => Array.empty[String])

  def calculate() = Future {
    val moviesInfoConfig = ReadConfig(Map("collection" -> "movies_infos"), Some(ReadConfig(sc)))
    val vectorSizeConfig = WriteConfig(Map("collection" -> "vector_size"), Some(WriteConfig(sc)))
    val writeConfig = WriteConfig(Map("collection" -> "vector_data"), Some(WriteConfig(sc)))

    val moviesInfoDF = MongoSpark
      .load(sc, moviesInfoConfig).toDF()
      .select("imdbID", "Actors", "Directors", "Genres", "Countries", "Title", "Plot", "Writers", "Productions", "Year", "imdbRating")
      .cache()

    val vecDF = calcMovieDataVecs(moviesInfoDF)

    val sizeDf = getVecSizeDf(vecDF)

    MongoSpark.save(sizeDf, vectorSizeConfig)
    MongoSpark.save(transformVectorsToMaps(vecDF), writeConfig)
  }

  def getVecSizeDf(vecDf: DataFrame): DataFrame = {
    val singleRecord = vecDf.rdd.take(1).last

    val sizeMap = Map[String, Int] (
      "Title" -> singleRecord.getAs[SparseVector]("TitleVec").size,
      "Plot" -> singleRecord.getAs[SparseVector]("PlotVec").size,
      "Genres" -> singleRecord.getAs[SparseVector]("GenresVec").size,
      "Actors" -> singleRecord.getAs[SparseVector]("ActorsVec").size,
      "Countries" -> singleRecord.getAs[SparseVector]("CountriesVec").size,
      "Writers" -> singleRecord.getAs[SparseVector]("WritersVec").size,
      "Directors" -> singleRecord.getAs[SparseVector]("DirectorsVec").size,
      "Productions" -> singleRecord.getAs[SparseVector]("ProductionsVec").size,
      "Decade" -> singleRecord.getAs[SparseVector]("DecadeVec").size
    )

    sizeMap.tail.foldLeft(Seq(sizeMap.head._2).toDF(sizeMap.head._1))((acc,curr) => acc.withColumn(curr._1,lit(curr._2)))
  }

  def calcMovieDataVecs(moviesInfoDf: DataFrame): DataFrame = {
    val df1 = createTitleVectors(moviesInfoDf).drop("Title")
    val df2 = createPlotVectors(df1).drop("Plot")
    val df3 = createFieldVector(df2, "Genres", "GenresVec").drop("Genres")
    val df4 = createFieldVector(df3, "Actors", "ActorsVec").drop("Actors")
    val df5 = createFieldVector(df4, "Countries", "CountriesVec").drop("Countries")
    val df6 = createFieldVector(df5, "Writers", "WritersVec").drop("Writers")
    val df7 = createFieldVector(df6, "Directors", "DirectorsVec").drop("Directors")
    val df8 = createFieldVector(df7, "Productions", "ProductionsVec").drop("Productions")
    val df9 = df8.withColumn("Decade", yearToDecadeUdf(col("Year"))).drop("Year")
    val df10 = createFieldVector(df9, "Decade", "DecadeVec")
    df10
  }

  def createFieldVector(moviesInfoDF: DataFrame, inputCol: String, outputCol: String): DataFrame = {
    val fieldCol = moviesInfoDF.col(inputCol)
    val fieldDF = moviesInfoDF.withColumn(inputCol, when(fieldCol.isNull, emptyArrayUdf()).otherwise(fieldCol))

    val cvModel: CountVectorizerModel = new CountVectorizer()
      .setInputCol(inputCol)
      .setOutputCol(inputCol+"1")
      .fit(fieldDF)

    val fieldVecDF = cvModel.transform(fieldDF)

    fieldVecDF.withColumn(outputCol, col(inputCol+"1")).drop(inputCol+"1")
  }

  def createTitleVectors(moviesInfoDF: DataFrame): DataFrame = {

    val titleCol = moviesInfoDF.col("Title")
    val titleDF = moviesInfoDF.withColumn("Title", when(titleCol.isNull, "").otherwise(titleCol))
    val tokenizer = new Tokenizer().setInputCol("Title").setOutputCol("TitleWords")
    val wordsData = tokenizer.transform(titleDF)

    val hashingTF = new HashingTF()
      .setInputCol("TitleWords").setOutputCol("TitleFeatures")

    val featurizedData = hashingTF.transform(wordsData)

    val idf = new IDF().setInputCol("TitleFeatures").setOutputCol("TitleVec")
    val idfModel = idf.fit(featurizedData)


    val idfData = idfModel.transform(featurizedData).drop("TitleFeatures", "TitleWords")

    idfData
  }

  def createPlotVectors(moviesInfoDF: DataFrame): DataFrame = {

    val plotCol = moviesInfoDF.col("Plot")
    val plotDF = moviesInfoDF.withColumn("Plot", when(plotCol.isNull, "").otherwise(plotCol))
    val tokenizer = new Tokenizer().setInputCol("Plot").setOutputCol("PlotWords")
    val wordsData = tokenizer.transform(plotDF)

    val remover = new StopWordsRemover()
      .setInputCol("PlotWords")
      .setOutputCol("FilteredPlotWords")

    val filteredWordsData = remover.transform(wordsData)

    val hashingTF = new HashingTF()
      .setInputCol("FilteredPlotWords").setOutputCol("PlotFeatures")

    val featurizedData = hashingTF.transform(filteredWordsData)

    val idf = new IDF().setInputCol("PlotFeatures").setOutputCol("PlotVec")
    val idfModel = idf.fit(featurizedData)


    val idfData = idfModel.transform(featurizedData).drop("PlotFeatures", "PlotWords", "FilteredPlotWords")

    idfData
  }

  def transformVectorsToMaps(movieVecsDf: DataFrame): DataFrame = {
    movieVecsDf
      .withColumn("Title", sparseVectorToMapUdf(col("TitleVec"))).drop("TitleVec")
      .withColumn("Plot", sparseVectorToMapUdf(col("PlotVec"))).drop("PlotVec")
      .withColumn("Genres", sparseVectorToMapUdf(col("GenresVec"))).drop("GenresVec")
      .withColumn("Actors", sparseVectorToMapUdf(col("ActorsVec"))).drop("ActorsVec")
      .withColumn("Countries", sparseVectorToMapUdf(col("CountriesVec"))).drop("CountriesVec")
      .withColumn("Writers", sparseVectorToMapUdf(col("WritersVec"))).drop("WritersVec")
      .withColumn("Directors", sparseVectorToMapUdf(col("DirectorsVec"))).drop("DirectorsVec")
      .withColumn("Productions", sparseVectorToMapUdf(col("ProductionsVec"))).drop("ProductionsVec")
      .withColumn("Decade", sparseVectorToMapUdf(col("DecadeVec"))).drop("DecadeVec")
  }
}
