/**
  * Created by baunov on 26/03/2018.
  */

package baunov.recommender
import baunov.recommender.controller.RecommenderController
import utils.Logging

object Server extends InitSpark  with Logging  {

  def main(args: Array[String]): Unit = {
    run()
  }

  private def run(): Unit = {
    try {
      RecommenderController.run(sys.env.getOrElse("PORT", "5000").toInt)
    }
    catch {
      case e: Exception =>
        logger.error("Error executing RecommenderServerApp", e)
        sys.exit(1)
    }
  }
}
