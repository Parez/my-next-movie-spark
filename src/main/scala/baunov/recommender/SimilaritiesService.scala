package baunov.recommender

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.{ReadConfig, WriteConfig}
import org.apache.spark.sql.functions.col
import Main.{sc, sparkSession}
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.functions._
import baunov.recommender.model.{MovieData, MovieVecData, Similarity}
import sparkSession.implicits._
import scala.collection.Map
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by baunov on 20/03/2018.
  */
object SimilaritiesService {

  def calculate(sparseVecMoviesData: Dataset[MovieVecData], movieId: Int) = Future {

    System.out.println("Start similarities calculations")

    val writeConfig = WriteConfig(Map("collection" -> "similarities"), Some(WriteConfig(sc)))

    val mainMovie: MovieVecData = sparseVecMoviesData.where(col("imdbID") === movieId).first()

    val finalDf: Dataset[Similarity] = calculateScores(mainMovie, sparseVecMoviesData.filter(col("imdbID") =!= mainMovie.imdbID))
        .map(scores => Similarity(mainMovie.imdbID, scores.imdbID, scores.score))
        .orderBy(desc("score")).limit(MAX_SIMILARITIES)

    MongoSpark.save(finalDf, writeConfig)
  }
}
